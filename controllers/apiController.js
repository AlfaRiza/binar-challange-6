const { user_game, pvp_room } = require('../models')
const format = (user) => {
    const { id, username } = user
    return {
        id,
        username,
        accessToken : user.generateToken()
    }
}

const isP1Win = (p1Hand, p2Hand) => {
    if (p1Hand === p2Hand) {
        return 0;
    } if ((p1Hand === 'R' && p2Hand === 'S') || (p1Hand === 'P' && p2Hand === 'R') || (p1Hand === 'S' && p2Hand === 'P')){
        return 1;
    } if ((p1Hand === 'S' && p2Hand === 'R' || (p1Hand === 'R' && p2Hand === 'P') || (p1Hand === 'P' && p2Hand === 'S'))){
        return -1
    } 
}

module.exports = {
    login: (req, res) => {
        user_game.authenticate(req.body)
        .then(user => {
            res.json(format(user))
        })
        console.log('data', req.body)
    },
    me: (req, res) => {
        const {username, isSuperAdmin, createdAt} = req.user.dataValues
        res.json({username, isSuperAdmin, createdAt})
    },
    createRoom: (req, res) => {
        // get player id
        const {id} = req.user.dataValues

        pvp_room.create({
            p1_id: id
        }).then(room => {
            res.json({
                roomId: room.id
            })
        })
    },
    join: (req, res) => {
        const { roomId } = req.params
        const {id} = req.user.dataValues

        pvp_room.findOne({
            where:{
                id: roomId
            }
        }).then(room => {
            if (room.p2_id != null) {
                return res.status(422).json({
                    message: 'player 2 already joined'
                })
            }
    
            pvp_room.update({
                p2_id: id
            }, {
                where: {id: roomId}
            }).then(() => {
                res.json({message: 'success join'})
            })
        })

        

    },
    fight: async (req, res) => {
        const { roomId } = req.params
        const {id} = req.user.dataValues
        const {firstHand, secondHand, thirdHand} = req.body
        const isP1 = false
        const isP2 = false

        if(!firstHand || !secondHand || !thirdHand) {
            return res.json({
                message: 'pick your choice'
            })
        }

        const room = await pvp_room.findOne({
            where:{
                id: roomId
            }
        })

        // res.send(room);

        if (room === null) {
            return res.status(404).json({
                message: 'room id not found'
            })
        }

        if (room.winner_id != null) {
            return res.status(422).json({
                message: 'game has ended'
            })
        }

        if (room.p1_id == id)
            isP1 = true

        if (room.p2_id == id)
            isP2 = false
        
        if (!isP1 && !isP2) {
            return res.status(401).json({
                message: 'unauthorized player detected'
            })
        }

        if (isP1) {
            pvp_room.update({
                p1_firstHand: firstHand,
                p1_secondHand: secondHand,
                p1_thirdHand: thirdHand
            }).then(() => {
                res.json({
                    message: 'success'
                })
            })
        }

        if (isP2) {
            pvp_room.update({
                p2_firstHand: firstHand,
                p2_secondHand: secondHand,
                p2_thirdHand: thirdHand
            }).then(() => {
                res.json({
                    message: 'success'
                })
            })
        }
        // if (room.p1_id != id || (room.p2_id != null && room.p2_id != id)){
        //     return res.status(401).json({
        //         message: 'unauthorized player detected'
        //     })
        // }


    },
    winner: async (req, res) => {
        const { roomId } = req.params

        const room = await pvp_room.findOne({
            where:{
                id: roomId
            }
        })

        let balancer = 0
        balancer = balancer + isP1Win(room.p1_firstHand, room.p2_firstHand)
        balancer = balancer + isP1Win(room.p1_secondHand, room.p2_secondHand)
        balancer = balancer + isP1Win(room.p1_thirdHand, room.p2_thirdHand)

        if(balancer === 0) {
            return 'Seri'
        } 
        if (balancer > 0) {
            pvp_room.update({
                winner_id: room.p1_id
            }).then(() => {
                res.json({
                    message: 'P1 WIn'
                })
            })
        } 
        if (balancer < 0) {
            pvp_room.update({
                winner_id: room.p2_id
            }).then(() => {
                res.json({
                    message: 'P2 WIn'
                })
            })
        }
    },
}