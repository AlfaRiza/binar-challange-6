const { user_game, user_game_biodata } = require('../models');
const passport = require('../lib/passport')
module.exports = {
    viewLogin: (req, res) => {
        res.render('login')
    },
    viewRegister: (req, res) => {
        res.render('register')
    },
    login: passport.authenticate('local', {
        successRedirect: '/dashboard',
        failureRedirect: '/login',
        failureFlash: true // Untuk mengaktifkan express flash
    }),
    register: (req, res) => {
        let { 
            username, 
            password, 
            first_name, 
            last_name, 
            birthplace 
        } = req.body;
        const data = {
            username, 
            password, 
        }
        user_game.register({
            username: username,
            password: password,
        }).then( (user_game) => {
            user_game_biodata.create({
                id_user: user_game.id,
                first_name,
                last_name,
                birthplace
            })
            .then(response => {
                res.redirect('/login')
            })
        })
    }
}