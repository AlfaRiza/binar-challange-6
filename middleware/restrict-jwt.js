const passportJWT = require('../lib/passport-jwt')

module.exports = passportJWT.authenticate('jwt', {
    session: false,
})