// middlewares/restrict.js
module.exports = (req, res, next) => {
    if (req.isAuthenticated()) return next()
    // Bila tidak, kita akan redirect ke halaman login
    res.redirect('/login')
}