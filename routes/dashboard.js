const routes = require('express').Router()
const { viewDashboard } = require('../controllers/dashboardController')
const restrict = require('../middleware/restrict')

routes.get('/dashboard', viewDashboard)

module.exports = routes;