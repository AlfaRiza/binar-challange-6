const routes = require('express').Router();
const {viewHome} = require('../controllers/homePageController');

routes.get('/', viewHome);

module.exports = routes;