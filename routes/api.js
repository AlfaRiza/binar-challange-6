const routes = require('express').Router()
const { login, me, createRoom, fight, winner, join } = require('../controllers/apiController')
const restrictJWT = require('../middleware/restrict-jwt')

routes.post('/login', login)
routes.get('/me', restrictJWT, me)

routes.post('/create-room', restrictJWT, createRoom)
routes.post('/join/:roomId', restrictJWT, join)

routes.post('/fight/:roomId', restrictJWT, fight)

routes.get('/winner/:roomId', restrictJWT, winner)

module.exports = routes