const routes = require('express').Router();
const { viewLogin, viewRegister, login, register } = require('../controllers/authController');

routes.get('/login', viewLogin);

routes.post('/login', login);

routes.get('/register', viewRegister);

routes.post('/register', register);

module.exports = routes;